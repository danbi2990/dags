from datetime import datetime

from airflow import DAG
from airflow.operators.bash_operator import BashOperator

from common import ASSEMBLY_PROJECT, ASSEMBLY_PYTHON

dag = DAG('watch_0',
          schedule_interval='0 12 * * *',
          start_date=datetime(2017, 3, 20), catchup=False)

bash_command = f'{ASSEMBLY_PYTHON} {ASSEMBLY_PROJECT}/scrape/watch_committee\
_step0.py'

task0 = BashOperator(task_id='watch_committee_step0',
                     bash_command=bash_command, dag=dag)
